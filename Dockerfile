FROM node:16-alpine

WORKDIR /packages/server
COPY /packages/server/package.json ./
COPY /packages/server/yarn.lock ./
RUN yarn

COPY /packages/server/src src
COPY /packages/server/tsconfig.json ./
RUN yarn build

EXPOSE 3000

CMD ["yarn", "start:dev"]
