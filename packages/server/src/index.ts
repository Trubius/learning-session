import fastify from 'fastify';

const server = fastify();

server.get('/', async () => {
  return 'server is running';
});

server.get('/first', async () => {
  return 'first response';
});

server.listen({ port: 3000, host: '0.0.0.0' }, (err, address) => {
  if (err) {
    console.error(err);
    process.exit(1);
  }
  console.log(`Server listening at ${address}`);
});
